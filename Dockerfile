FROM nginx

ARG env=prod

MAINTAINER Patrick Yoon "me@patyoon.com"

# Define working directory.
WORKDIR /etc/nginx

# Copy all config files

COPY conf.d/${env}.conf /etc/nginx/conf.d/
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY snippets/ /etc/nginx/snippets/

CMD ["nginx"]
