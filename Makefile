REPO    = patyoon/nginx
VERSION = 0.0.1

all: push

push: build
	docker push ${REPO}:${VERSION}
push-dev: build
	docker push ${REPO}:dev
build:
	docker build --tag=${REPO}:${VERSION} $$PWD
build-dev:
	docker build --tag=${REPO}:dev --build-arg env=dev $$PWD
